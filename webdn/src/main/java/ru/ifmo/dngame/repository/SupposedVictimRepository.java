package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.SupposedVictim;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SupposedVictimRepository extends JpaRepository<SupposedVictim, Integer> {

}
