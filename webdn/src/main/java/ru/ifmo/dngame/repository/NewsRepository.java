package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.News;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {

    @Query("select n from News n where n.kira.id=:kiraId and n.agent.id = :agentId")
    List<News> findAllByKiraAndAgent(@Param("kiraId") Integer kiraId, @Param("agentId") Integer agentId);

    @Query("select n from News n where n.agent.id = :agentId")
    List<News> findAllByAgentId(@Param("agentId") Integer agentId);

    @Transactional
    @Modifying
    @Query("delete from News n where n.kira.id = :kiraId and n.agent.id = :agentId")
    void deleteAllByKiraIdAndAgentId(@Param("kiraId") Integer kiraId, @Param("agentId") Integer agentId);

}
