package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.Kira;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface KiraRepository extends JpaRepository<Kira, Integer> {

    @Query("select k.points from Kira k where k.id = :id")
    Integer findPointsById(@Param("id") Integer id);

    @Query("select r.id from Kira k join k.region r where r.country=:country and r.city=:city")
    Integer findRegionsIdByCountryAndCity(@Param("country") String country, @Param("city") String city);

    @Transactional
    @Modifying
    @Query("update Kira k set k.region.id=:regionId where k.id = :kiraId")
    void setRegion(@Param("kiraId") Integer kiraId, @Param("regionId") Integer regionId);

}
