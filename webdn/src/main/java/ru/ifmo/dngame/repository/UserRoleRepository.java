package ru.ifmo.dngame.repository;

import org.springframework.stereotype.Repository;
import ru.ifmo.dngame.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

}
