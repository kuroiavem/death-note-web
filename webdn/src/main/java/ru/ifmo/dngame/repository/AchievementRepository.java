package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.Achievement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface AchievementRepository extends JpaRepository<Achievement, String> {

    @Query("select a from Achievement a left join a.kiras kira left join kira.user user where user.id =:userId")
    List<Achievement> findAllByUserIdIfKirasIs(@Param("userId") Integer userId);

    @Query("select a from Achievement a left join a.agents agent left join agent.user user where user.id=:userId")
    List<Achievement> findAllByUserIdIfAgentsIs(@Param("userId") Integer userId);

    @Query("select a from Achievement a where a.isKiras =:isKiras")
    List<Achievement> findAllByIsKiras(@Param("isKiras") Boolean isKiras);

}
