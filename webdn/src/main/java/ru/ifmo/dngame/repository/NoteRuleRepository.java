package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.NoteRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface NoteRuleRepository extends JpaRepository<NoteRule, Integer> {

}
