package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.DeathReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeathReasonRepository extends JpaRepository<DeathReason, Integer> {

}
