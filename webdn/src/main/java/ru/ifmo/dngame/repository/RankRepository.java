package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.Rank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface RankRepository extends JpaRepository<Rank, Integer> {

    @Query("select r.rank from Rank r left join r.agents a left join a.user u where u.id = :userId")
    String findRankByUserIdIfIsAgents(@Param("userId") Integer userId);

    @Query("select r.rank from Rank r left join r.kiras k left join k.user u where u.id = :userId")
    String findRankByUserIdIfIsKiras(@Param("userId") Integer userId);

}
