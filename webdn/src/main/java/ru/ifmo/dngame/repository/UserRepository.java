package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    @Transactional
    @Modifying
    @Query("update User u set u.password = :passw, u.id = :userId")
    void changePassword(@Param("passw") String passw, @Param("userId") Integer userId);

    @Transactional
    @Modifying
    @Query("update User u set u.kira.id=:kiraId")
    void addKira(@Param("kiraId") Integer kiraId);

    @Transactional
    @Modifying
    @Query("update User u set u.agent.id=:agentId")
    void addAgent(@Param("agentId") Integer agentId);

    User findByLogin(String login);


}
