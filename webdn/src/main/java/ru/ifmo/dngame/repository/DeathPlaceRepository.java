package ru.ifmo.dngame.repository;

import ru.ifmo.dngame.entity.DeathPlace;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DeathPlaceRepository extends JpaRepository<DeathPlace, Integer> {

}
