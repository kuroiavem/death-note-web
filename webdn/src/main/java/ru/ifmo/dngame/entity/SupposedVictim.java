package ru.ifmo.dngame.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Simple JavaBean object that represents role of {@link SupposedVictim},
 * describes supposed victim for agent's fake news.
 */


@Data
@Entity
@Table(name = "supposed_victim")
public class SupposedVictim implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "name", length = 55)
    private String name;

    @Column(name = "surname", length = 55)
    private String surname;

    @Column(name = "patronymic", length = 55)
    private String patronymic;

    @OneToOne(mappedBy = "supposedVictim", cascade = CascadeType.ALL)
    private News news;

}
