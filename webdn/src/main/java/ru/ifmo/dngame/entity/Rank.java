package ru.ifmo.dngame.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Simple JavaBean object that represents role of {@link Rank},
 * describes the rank of player.
 */


@Data
@Entity
@Table(name = "rank")
public class Rank implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "level", nullable = false)
    private Integer level;

    @Column(name = "rank", length = 50)
    private String rank;

    @OneToMany(mappedBy = "rank")
    private Collection<Agent> agents;

    @OneToMany(mappedBy = "rank")
    private Collection<Kira> kiras;

}
