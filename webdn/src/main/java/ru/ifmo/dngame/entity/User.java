package ru.ifmo.dngame.entity;

import lombok.Data;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.Collection;

/**
 * Simple JavaBean object that represents role of {@link User}.
 */


@Data
@Entity
@Table(name = "users")
public class User implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "nickname", nullable = false, length = 30, unique = true)
    private String nickname;

    @Column(name = "login", nullable = false, length = 30, unique = true)
    private String login;

    @Column(name = "password", nullable = false)
    private String password;

    @Transient
    private String confirmPassword;

    @Column(name = "email", nullable = false, length = 200, unique = true)
    private String email;

    @Column(name = "registration_date", nullable = false)
    private Date registrationDate;

    @Column(name = "last_visit_timestamp")
    private Date lastVisitTime;

    @ManyToMany
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Collection<UserRole> roles;

    @OneToOne
    @JoinColumn(name = "agent_id")
    private Agent agent;

    @OneToOne
    @JoinColumn(name = "kira_id")
    private Kira kira;

    @ManyToOne
    @JoinColumn(name = "user_status")
    private UserStatus userStatus;

    public User(String nickname, String login, String password, String email){
        this.nickname = nickname;
        this.login = login;
        this.password = password;
        this.email = email;
        Date date = new Date(System.currentTimeMillis());
        this.registrationDate = date;
        this.lastVisitTime = date;
    }

    public User() {

    }
}
