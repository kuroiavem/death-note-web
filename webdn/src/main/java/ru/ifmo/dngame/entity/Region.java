package ru.ifmo.dngame.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Simple JavaBean object that represents role of {@link Region},
 * describes region in the game world.
 */


@Data
@Entity
@Table(name = "regions")
public class Region implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "country")
    private String country;

    @Column(name = "city")
    private String city;

    @OneToMany(mappedBy = "region")
    private Collection<Agent> agents;

    @OneToMany(mappedBy = "region")
    private Collection<Kira> kiras;

    @OneToMany(mappedBy = "whereDid")
    private Collection<News> whereNews;

    @OneToMany(mappedBy = "distributionRegion")
    private Collection<News> distributionNews;

}
