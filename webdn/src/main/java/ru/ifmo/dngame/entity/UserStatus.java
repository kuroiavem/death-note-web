package ru.ifmo.dngame.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Simple JavaBean object that represents role of {@link UserStatus},
 * describes the status of user(banned or not).
 */

@Data
@Entity
@Table(name = "user_statuses")
public class UserStatus implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "status", length = 50, unique = true)
    private String status;

    @OneToMany(mappedBy = "userStatus")
    private Collection<User> users;

}
