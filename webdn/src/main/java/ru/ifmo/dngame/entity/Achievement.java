package ru.ifmo.dngame.entity;

import lombok.Data;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Collection;

/**
 * Simple JavaBean object that represents role of {@link Achievement}.
 */


@Data
@Entity
@Table(name = "achievements")
public class Achievement implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @Column(name = "title", nullable = false, length = 50, unique = true)
    private String title;

    @Column(name = "task")
    private String task;

    @Column(name = "description")
    private String description;

    @Column(name = "is_kiras")
    private Boolean isKiras;

    @ManyToMany(mappedBy = "achievements")
    private Collection<Agent> agents;

    @ManyToMany(mappedBy = "achievements")
    private Collection<Kira> kiras;

}
