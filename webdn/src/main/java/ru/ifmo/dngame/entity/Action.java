package ru.ifmo.dngame.entity;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Simple JavaBean object that represents role of {@link Action},
 * describes the action that is in news.
 */


@Data
@Entity
@Table(name = "actions")
public class Action implements Serializable {

    private static final long serialVersionUID = -32411321342135L;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    private Integer id;

    @Column(name = "description", length = 55)
    private String description;

    @OneToOne(mappedBy = "action", cascade = CascadeType.ALL)
    private News news;

}
