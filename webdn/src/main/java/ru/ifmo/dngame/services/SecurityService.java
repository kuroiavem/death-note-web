package ru.ifmo.dngame.services;

/**
 * Service for Security
 */

public interface SecurityService {

    String findLoggedInLogin();
    void autoLogin(String login, String password);

}
