package ru.ifmo.dngame.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.AgentRepository;
import ru.ifmo.dngame.services.AgentService;

/**
 * Implementation of {@link AgentService} interface.
 */
@Service
public class AgentServiceImpl implements AgentService {

    public AgentServiceImpl(ClassPathXmlApplicationContext context) {
        this.agentRepository = context.getBean(AgentRepository.class);
    }

    public AgentServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.Agent}.
     */
    @Autowired
    private AgentRepository agentRepository;

    public void setRepository(AgentRepository agentRepository){
        this.agentRepository = agentRepository;
    }

    /**
     * Get points.
     * @param agentId agent id
     * @return points
     */
    @Override
    public Integer getPoints(Integer agentId) {
        return agentRepository.findPointsById(agentId);
    }

    /**
     * Set home region to agent.
     * @param agentId agent id
     * @param country country name
     * @param city city name
     */
    @Override
    public void setRegion(Integer agentId, String country, String city) {
        Integer regionId = agentRepository.findRegionsIdByCountryAndCity(country, city);
        agentRepository.setRegionByAgentIdAndRegionId(agentId, regionId);
    }
}
