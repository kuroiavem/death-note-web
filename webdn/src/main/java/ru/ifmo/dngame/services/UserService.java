package ru.ifmo.dngame.services;

import ru.ifmo.dngame.entity.User;

import java.util.Collection;

public interface UserService {

    Collection<String> getRating();
    void chooseClass(Boolean isKira);
    void changePassword(String password, Integer userId);
    void saveUser(User user);
    User findByLogin(String login);

}
