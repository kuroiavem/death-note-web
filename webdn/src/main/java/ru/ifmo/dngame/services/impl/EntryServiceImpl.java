package ru.ifmo.dngame.services.impl;

import ru.ifmo.dngame.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.EntryRepository;
import ru.ifmo.dngame.services.EntryService;
import java.util.Collection;

/**
 * Implementation of {@link EntryService} interface
 * for work with entries.
 */

@Service
public class EntryServiceImpl implements EntryService {

    public EntryServiceImpl(ClassPathXmlApplicationContext context) {
        this.entryRepository = context.getBean(EntryRepository.class);
    }

    public EntryServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.Entry}.
     */
    @Autowired
    private EntryRepository entryRepository;

    public void setRepository(EntryRepository entryRepository){
        this.entryRepository = entryRepository;
    }

    /**
     * Open note(get all entries that are written in the note) of the Kira.
     * @param kiraId kira id
     * @return collection of all the entries in the kira's note
     */
    @Override
    public Collection<Entry> openNote(Integer kiraId) {
        return entryRepository.openNote(kiraId);
    }

    /**
     * Get all entries that are written in the note of the Kira.
     * @param kiraId kira id
     * @return collection of all the entries in the kira's note
     */
    @Override
    public Collection<Entry> openListOfVictims(Integer kiraId) {
        return entryRepository.openNote(kiraId);
    }

    /**
     * Write new entry in the Kira's note.
     * @param entry entry
     */
    @Override
    public void addEntry(Entry entry) {
        entryRepository.save(entry);
    }
}
