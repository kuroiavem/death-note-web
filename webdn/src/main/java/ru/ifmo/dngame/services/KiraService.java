package ru.ifmo.dngame.services;

public interface KiraService {

    Integer getPoints(Integer kiraId);
    void setRegion(Integer kiraId, String country, String city);

}
