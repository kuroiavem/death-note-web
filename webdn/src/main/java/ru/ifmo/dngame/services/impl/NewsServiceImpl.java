package ru.ifmo.dngame.services.impl;

import ru.ifmo.dngame.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.NewsRepository;
import ru.ifmo.dngame.services.NewsService;

import java.util.Collection;

/**
 * Implementation of {@link NewsService} interface.
 */

@Service
public class NewsServiceImpl implements NewsService {

    public NewsServiceImpl(ClassPathXmlApplicationContext context) {
        this.newsRepository = context.getBean(NewsRepository.class);
    }

    public NewsServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.News}.
     */
    @Autowired
    private NewsRepository newsRepository;

    public void setRepository(NewsRepository newsRepository){
        this.newsRepository = newsRepository;
    }

    /**
     * Get news(see news) for the current game match.
     * @param kiraId kira id
     * @param agentId agent id
     * @return collection of news
     */
    @Override
    public Collection<News> getNews(Integer kiraId, Integer agentId) {
        return newsRepository.findAllByKiraAndAgent(kiraId, agentId);
    }

    /**
     * Add news.
     * @param news news entity
     */
    @Override
    public void addNews(News news) {
        newsRepository.save(news);
    }

    /**
     * Open tablet/Get all news by Agent id.
     * @param agentId agent id
     * @return collection of news
     */
    @Override
    public Collection<News> openTablet(Integer agentId) {
        return newsRepository.findAllByAgentId(agentId);
    }

    /**
     * Delete news from the last match.
     * @param kiraId kira id
     * @param agentId agent id
     */
    @Override
    public void deleteNewsOfTheLastGame(Integer kiraId, Integer agentId) {
        newsRepository.deleteAllByKiraIdAndAgentId(kiraId, agentId);
    }
}
