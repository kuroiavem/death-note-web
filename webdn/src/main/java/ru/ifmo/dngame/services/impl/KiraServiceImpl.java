package ru.ifmo.dngame.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.KiraRepository;
import ru.ifmo.dngame.services.KiraService;

/**
 * Implementation of {@link KiraService} interface.
 */

@Service
public class KiraServiceImpl implements KiraService {

    public KiraServiceImpl(ClassPathXmlApplicationContext context) {
        this.kiraRepository = context.getBean(KiraRepository.class);
    }

    public KiraServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.Kira}.
     */
    @Autowired
    private KiraRepository kiraRepository;

    public void setRepository(KiraRepository kiraRepository){
        this.kiraRepository = kiraRepository;
    }

    /**
     * Get points of the kira.
     * @param kiraId kira id
     * @return value of points
     */
    @Override
    public Integer getPoints(Integer kiraId) {
        return kiraRepository.findPointsById(kiraId);
    }

    /**
     * Set region that the Kira lives in.
     * @param kiraId kira id
     * @param country country from the game world
     * @param city city from the game world
     */
    @Override
    public void setRegion(Integer kiraId, String country, String city) {
        Integer regionId = kiraRepository.findRegionsIdByCountryAndCity(country, city);
        kiraRepository.setRegion(kiraId, regionId);
    }
}
