package ru.ifmo.dngame.services;

import ru.ifmo.dngame.entity.*;

import java.util.Collection;

public interface NewsService {

    Collection<News> getNews(Integer kiraId, Integer agentId);
    void addNews(News news);
    Collection<News> openTablet(Integer agentId);
    void deleteNewsOfTheLastGame(Integer kiraId, Integer agentId);

}
