package ru.ifmo.dngame.services.impl;

import org.springframework.security.authentication.AuthenticationManager;
import ru.ifmo.dngame.entity.Agent;
import ru.ifmo.dngame.entity.Kira;
import ru.ifmo.dngame.entity.User;
import ru.ifmo.dngame.entity.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.AgentRepository;
import ru.ifmo.dngame.repository.KiraRepository;
import ru.ifmo.dngame.repository.UserRepository;
import ru.ifmo.dngame.repository.UserRoleRepository;
import ru.ifmo.dngame.services.UserService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Implementation of {@link UserService} interface.
 */

@Service
public class UserServiceImpl implements UserService{

    public UserServiceImpl(ClassPathXmlApplicationContext context) {
        this.userRepository = context.getBean(UserRepository.class);
    }

    public UserServiceImpl(){}

    /**
     * Repository for {@link User}.
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * Repository for {@link UserRole}.
     */
    @Autowired
    private UserRoleRepository roleRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    /**
     * Repository for {@link Kira}.
     */
    @Autowired
    private KiraRepository kiraRepository;

    /**
     * Repository for {@link Agent}.
     */
    @Autowired
    private AgentRepository agentRepository;

    public void setRepository(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    /**
     * Get global rating.
     * @return string(nickname|points)
     */
    @Override
    public Collection<String> getRating() {
        List<User> users = userRepository.findAll();
        List<String> usersRanking = new ArrayList<String>();
        List<String> usersName = new ArrayList<String>();
        List<Integer> usersPoints = new ArrayList<Integer>();
        int i = 0;
        for (User user: users){
            usersPoints.add(i, user.getKira().getPoints()+user.getAgent().getPoints());
            usersName.add(i, user.getNickname());
        }

        for (int j =0; j<i; j++){
            usersRanking.add(j, usersName.get(j)+ "|" + String.valueOf(usersPoints.get(j)));
        }
        return usersRanking;
    }

    /**
     * Chose class.
     * @param isKira If isKira = true -> chose kira's class, if not -> chose agent's class
     */
    @Override
    public void chooseClass(Boolean isKira) {
        if (isKira){
            Kira kira = new Kira();
            kiraRepository.save(kira);
            userRepository.addKira(kira.getId());
        } else {
            Agent agent = new Agent();
            agentRepository.save(agent);
            userRepository.addAgent(agent.getId());
        }
    }

    /**
     * Change password of the user.
     * @param password password.
     * @param userId user id.
     */
    @Override
    public void changePassword(String password, Integer userId) {
        userRepository.changePassword(password, userId);
    }

    /**
     * Sign up.
     * @param user User entity
     */
    @Override
    public void saveUser(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Collection<UserRole> roles = new HashSet<UserRole>();
        roles.add(roleRepository.getOne(1));
        user.setRoles(roles);
        userRepository.save(user);
    }

    /**
     * Get User entity by login.
     * @param login login
     * @return User entity
     */
    @Override
    public User findByLogin(String login) {
        return userRepository.findByLogin(login);
    }


}
