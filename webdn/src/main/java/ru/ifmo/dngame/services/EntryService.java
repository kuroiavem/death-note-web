package ru.ifmo.dngame.services;

import ru.ifmo.dngame.entity.*;

import java.util.Collection;

public interface EntryService {

    Collection<Entry> openNote(Integer kiraId);
    Collection<Entry> openListOfVictims(Integer kiraId);
    void addEntry(Entry entry);

}
