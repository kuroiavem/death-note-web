package ru.ifmo.dngame.services.impl;

import ru.ifmo.dngame.entity.Achievement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.AchievementRepository;
import ru.ifmo.dngame.services.AchievementService;
import java.util.Collection;

/**
 * Implementation of {@link AchievementService} interface
 * for game achievements.
 */
@Service
public class AchievementServiceImpl implements AchievementService {

    public AchievementServiceImpl(ClassPathXmlApplicationContext context){
           this.achievementRepository = context.getBean(AchievementRepository.class);
    }

    public AchievementServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.Achievement}.
     */
    @Autowired
    private AchievementRepository achievementRepository;

    public void setRepository(AchievementRepository achievementRepository){
        this.achievementRepository = achievementRepository;
    }

    /**
     * Add new achievement.
     * @param achievement Achievement object to save
     */
    @Override
    public void addAchievement(Achievement achievement) {
        achievementRepository.save(achievement);
    }

    /**
     * Get achievements of Kira/Agent by user id.
     * @param userId Id of user
     * @param isKira If isKira = true -> get kira's achievements, if not -> get agent's achievements
     * @return
     */
    @Override
    public Collection<Achievement> getCurrentAchievements(Integer userId, Boolean isKira) {
        if (isKira) {
            return achievementRepository.findAllByUserIdIfKirasIs(userId);
        } else {
            return achievementRepository.findAllByUserIdIfAgentsIs(userId);
        }
    }

    //

    /**
     * Get all achievements that can get a given class(Kira/Agent).
     * @param isKira If isKira = true -> get kira's achievements, if not -> get agent's achievements
     * @return achievements collection
     */
    @Override
    public Collection<Achievement> getAllAchievementsByClass(Boolean isKira) {
        return achievementRepository.findAllByIsKiras(isKira);
    }
}
