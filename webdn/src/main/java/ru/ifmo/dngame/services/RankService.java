package ru.ifmo.dngame.services;

public interface RankService {

    String getRank(Boolean isKira, Integer userId);

}
