package ru.ifmo.dngame.services;

public interface AgentService {

    Integer getPoints(Integer agentId);
    void setRegion(Integer agentId, String country, String city);
}
