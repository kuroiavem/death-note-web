package ru.ifmo.dngame.services;

import ru.ifmo.dngame.entity.Achievement;

import java.util.Collection;

public interface AchievementService {

    void addAchievement(Achievement achievement);
    Collection<Achievement> getCurrentAchievements(Integer userId, Boolean isKira);
    Collection<Achievement> getAllAchievementsByClass(Boolean isKira);

}
