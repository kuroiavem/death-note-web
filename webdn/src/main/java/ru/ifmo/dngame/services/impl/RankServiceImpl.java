package ru.ifmo.dngame.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import ru.ifmo.dngame.repository.RankRepository;
import ru.ifmo.dngame.services.RankService;

/**
 * Implementation of {@link RankService} interface.
 */

@Service
public class RankServiceImpl implements RankService {

    public RankServiceImpl(ClassPathXmlApplicationContext context) {
        this.rankRepository = context.getBean(RankRepository.class);
    }

    RankServiceImpl(){}

    /**
     * Repository for {@link ru.ifmo.dngame.entity.Rank}.
     */
    @Autowired
    private RankRepository rankRepository;

    public void setRepository(RankRepository rankRepository){
        this.rankRepository = rankRepository;
    }

    //Посмотреть ранк

    /**
     * See rank of the user's class.
     * @param isKira If isKira = true -> get kira's rank, if not -> get agent's rank
     * @param userId user id
     * @return the rank
     */
    @Override
    public String getRank(Boolean isKira, Integer userId) {
        if (isKira)
            return rankRepository.findRankByUserIdIfIsKiras(userId);
        else
            return rankRepository.findRankByUserIdIfIsAgents(userId);
    }
}
