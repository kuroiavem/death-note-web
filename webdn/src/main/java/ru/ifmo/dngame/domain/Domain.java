package ru.ifmo.dngame.domain;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.ifmo.dngame.services.impl.*;

import javax.persistence.EntityManagerFactory;

public class Domain {

    private static EntityManagerFactory emf;

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");


        //Test code
        AchievementServiceImpl achievementService = new AchievementServiceImpl(ctx);
        AgentServiceImpl agentService = new AgentServiceImpl(ctx);
        EntryServiceImpl entryService = new EntryServiceImpl(ctx);
        KiraServiceImpl kiraService = new KiraServiceImpl(ctx);
        NewsServiceImpl newsService = new NewsServiceImpl(ctx);
        RankServiceImpl rankService = new RankServiceImpl(ctx);
        UserServiceImpl userService = new UserServiceImpl(ctx);

    }
}
