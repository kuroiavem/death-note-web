package ru.ifmo.dngame.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ifmo.dngame.entity.User;

@RestController
public class EmailController {

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendMail(String subject, User to, String text){
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setFrom("kuroiavem@gmail.com");
        simpleMailMessage.setTo(to.getEmail());
        simpleMailMessage.setText(text);
        javaMailSender.send(simpleMailMessage);
    }

    public void sendMail(String subject, String from, String to, String text){
        SimpleMailMessage simpleMailMessage=new SimpleMailMessage();
        simpleMailMessage.setSubject(subject);
        simpleMailMessage.setFrom(from);
        simpleMailMessage.setTo(to);
        simpleMailMessage.setText(text);
        javaMailSender.send(simpleMailMessage);
    }

    @RequestMapping(path = "/mail", method = RequestMethod.POST)
    public String testMail(){
        sendMail("Test","kuroiavem@gmail.com", "kuroiavem@gmail.com","Test completed!");
        return "Email sent";
    }
}
