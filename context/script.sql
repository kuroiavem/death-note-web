create table persons(
	id serial primary key,
	name character(15) not null,
	surname character(20),
	patronymic character(20),
	sex boolean,
	born_date date not null,
	death_date date,
	check (death_date > born_date)
);

create table regions(
	id serial primary key,
	country text,
	city text
);

create table rank(
	id serial primary key,
	level integer not null,
	rank varchar(50)
);

create table user_statuses(
	id serial primary key,
	status varchar(50)
);

create table note_rules(
	id serial primary key,
	is_default boolean not null,
	description text
);


create table agents(
	person_id integer references persons(id) primary key,
	number_of_caught_killers integer default 0 check (number_of_caught_killers >= 0),
	region_id integer references regions(id) not null,
	rank_id integer references rank(id) not null,
	points integer default 0,
	check (points >= 0)
);

create table kira(
	person_id integer references persons(id) primary key,
	number_of_kills integer default 0 check (number_of_kills >=0),
	region_id integer references regions(id) not null,
	rank_id integer references rank(id) not null,
	points integer default 0,
	check (points >= 0)
);

create table note_rule_to_kira(
	kira_id integer references kira(person_id),
	rule_id integer references note_rules(id),
	PRIMARY KEY(kira_id, rule_id)
);

create table achievements(
	title varchar(50) primary key,
	task text,
	description text
);

create table achievements_to_agent(
	agent_id integer references agents(person_id),
	achievements_id varchar(50) references achievements(title),
	primary key (agent_id,achievements_id)
);

create table achievements_to_kira(
	kira_id integer references kira(person_id),
	achievements_id varchar(50) references achievements(title),
	primary key (kira_id,achievements_id)
);

create table news(
	id serial primary key,
	agent_id integer references agents(person_id),
	killer_id  integer references persons(id),
	distribution_region_id integer references regions(id),
	what_did varchar(500) not null,
	where_did_id integer references regions(id),
	over_whom_did_id integer references persons(id),
	publication_date date not null,
	publication_time time not null
);

create table descriptions(
	id serial primary key,
	description varchar(500)
);

create table death_places(
	id serial primary key,
	death_place varchar(500)
);

create table death_reasons(
	id serial primary key,
	death_reason varchar(500)
);

create table entries(
	id serial primary key,
	page_number integer not null,
	kira_id integer references kira(person_id),
	reason_id integer references death_reasons(id),
	death_place_id integer references death_places(id),
	description_id integer references descriptions(id),
	death_date date,
	death_time time,
	victim_id integer references persons(id)
);

CREATE EXTENSION chkpass;
CREATE TABLE users(
	id serial PRIMARY KEY,
	nickname varchar(30) not null,
	login varchar(30) not null,
	email varchar(200) not null,
	registration_date date not null,
	last_visit_timestamp timestamp,
	password chkpass,
	agent_id integer references agents(person_id) default null,
	kira_id integer references kira(person_id) default null,
	user_status integer references user_statuses(id)
);
